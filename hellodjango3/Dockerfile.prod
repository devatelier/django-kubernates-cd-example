# LARAsuite Dockerfile
# to build the docker image, call :
# docker build -t atilier/hellodjango3:0.0.1 .
# docker run -p 8000:8000 atilier/hellodjango3:0.0.1

FROM python:3.9-slim-buster as build_image

LABEL maintainer="mark doerr mark.doerr@uni-greifswald.de"
LABEL version="0.0.32"
LABEL description="LARAsuite Dockerfile \
    this will dockerise the complete LARAsuite into one single, handy docker"

#COPY lara_django_config.env /etc/lara_django_config.env

# Keeps Python from generating .pyc files in the container
#ENV PYTHONDONTWRITEBYTECODE 1

ENV LANG=C.UTF-8\
    PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1\
    PROJECT_ROOT=/opt/lara-django

ENV VIRTUAL_ENV=$PROJECT_ROOT \
    DJANGO_MEDIA_PATH=$PROJECT_ROOT\media \
    DEBIAN_FRONTEND=noninteractive 

# Debian package caching
ARG APT_PROXY
RUN set -x \
    && if [ "${APT_PROXY}" ]; \
    then echo "Acquire::http { Proxy \"http://${APT_PROXY}\"; };" > /etc/apt/apt.conf.d/01proxy \
    ; fi

RUN set -x \
    && adduser lara --disabled-password --disabled-login --no-create-home --gecos "" \
    # Create a virtualenv for the application dependencies.
    && python -V  # Print out python version for debugging

# install psycopg dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*

# to minimize image size one could build like (https://mherman.org/presentations/dockercon-2018/#39):
# FROM python:3.6 as base
# COPY requirements.txt /
# RUN pip wheel --no-cache-dir --no-deps --wheel-dir /wheels -r requirements.txt
# FROM python:3.6-alpine
# COPY --from=base /wheels /wheels
# COPY --from=base requirements.txt .
# RUN pip install --no-cache /wheels/* # flask, gunicorn, pycrypto
# see also: https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/

# to test: if user is used that everything is alreay owned by user ...
#USER lara
#WORKDIR ${PROJECT_ROOT}

# Install dependencies.
COPY requirements "${PROJECT_ROOT}/requirements/"

#RUN set -a \
#    && . /etc/lara_django_config.env \
#    && set +a \
#    && echo venv: ${DEFAULT_DATABASE_NAME} 

# Set virtualenv environment variables. This is equivalent to running
# source /env/bin/activate. This ensures the application is executed within
# the context of the virtualenv and will have access to its dependencies.
# s. https://pythonspeed.com/articles/activate-virtualenv-dockerfile/



RUN python3 -m venv $VIRTUAL_ENV 
# activating the virtual environment
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install --no-cache-dir --upgrade pip \
    && pip install --no-cache-dir -r $PROJECT_ROOT/requirements/production.txt

# psutil is needed by ARM builds otherwise gevent and gunicorn fail to start

# RUN UNAME=`uname -m` && if [ "${UNAME#*arm}" != $UNAME ]; then \
#     pip install --no-cache-dir \
#     psutil==$PYTHON_PSUTIL_VERSION \
# ; fi \
# && pip install --no-cache-dir lara-django \
# should be switchable, depending on ENV variable
# && pip install -r $PROJECT_ROOT/requirements/devel.txt

FROM build_image AS production

EXPOSE 8000

# Uncomment the following line to use NODB
# NODB will use SQLite and in-memory cache instead of Postgres And Redis
# This is useful to test the rest of the Django setup without configuring the database/cache
# Comment it out and rebuild this image once you have Postgres and Redis services in your cluster
# ENV NODB 1

# directory for static content
RUN mkdir -p /data/web/django-static \
    && chown lara:lara /data/web/django-static

# copy entrypoint.sh
# COPY ./entrypoint.sh .
#COPY ./entrypoint.prod.sh ${PROJECT_ROOT}/entrypoint.prod.sh
COPY docker-setup/init_and_run.sh ${PROJECT_ROOT}/init_and_run.sh

COPY hellodjango3/manage.py ${PROJECT_ROOT}
COPY hellodjango3/hellodjango3 "${PROJECT_ROOT}/hellodjango3/"
COPY hellodjango3/hello_app "${PROJECT_ROOT}/hello_app/"
COPY hellodjango3/todos "${PROJECT_ROOT}/todos/"

#COPY --chown=lara:lara log_project ${PROJECT_ROOT}/log_project/

# collect static in lara data web

RUN chown -R lara:lara ${PROJECT_ROOT}

WORKDIR ${PROJECT_ROOT}

RUN ls -Al
RUN ls -Al hellodjango3

USER lara

# CMD ["gunicorn", "--bind", "0.0.0.0:8000", "hellodjango3.wsgi"]
# run entrypoint.sh
#ENTRYPOINT ["${PROJECT_ROOT}/entrypoint.prod.sh"]
#ENTRYPOINT exec "${PROJECT_ROOT}/entrypoint.prod.sh"
#CMD exec "${PROJECT_ROOT}/init_and_run.sh"

# CMD python -m django --version
# CMD lara-django.py runserver 0.0.0.0:8080
