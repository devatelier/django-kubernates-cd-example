#!/usr/bin/env bash
cd /opt/lara-django
python3 manage.py init_django
#python3 manage.py initial_data
#/usr/local/bin/uwsgi --ini /etc/uwsgi/apps-enabled/uwsgi-app.ini
gunicorn hellodjango3.wsgi:application --bind 0.0.0.0:8000