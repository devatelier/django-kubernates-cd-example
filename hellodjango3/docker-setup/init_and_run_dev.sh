#!/usr/bin/env bash
cd /opt/lara-django
#python3 manage.py wait_for_db
#python3 manage.py migrate || { echo 'migrate failed' ; exit 1; }
#python3 manage.py collectstatic
#python3 manage.py init_admin
#python3 manage.py init_admin
python3 manage.py init_django
#python3 manage.py initial_data
#acd /home/docker
#/usr/local/bin/uwsgi --ini /etc/uwsgi/apps-enabled/uwsgi-app.ini
#gunicorn hellodjango3.wsgi:application --bind 0.0.0.0:8000
python manage.py runserver 0.0.0.0:8000