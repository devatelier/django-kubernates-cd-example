"""_____________________________________________________________________

:PROJECT: LARA

*lara_django setup *

:details: lara_people setup file for installation.
         - For installation, run:
           run pip3 install .
           or  python3 setup.py install

:file:    setup.py
:authors: mark doerr mark.doerr@uni-greifswald.de

:date: (creation)         20210507

.. note:: -
.. todo:: -
________________________________________________________________________
"""
__version__ = "0.0.45"

import os

from setuptools import setup, find_packages

package_name = 'hello_app'

# this is the lara base installtion with the core apps
# to exend the apps, configure lara full
install_requires = ['wheel', 'django', 'django-environ']

package_data = {}

data_files = []


def read(filename):
    with open(os.path.join(os.path.dirname(__file__), filename), 'r') as file:
        return file.read().strip()


setup(name=package_name,
      version=__version__,
      description='LARA hello_app - (lara.uni-greifswald.de/larasuite) ',
      long_description=read('README.rst'),
      long_description_content_type='text/x-rst',
      author='mark doerr',
      author_email='mark.doerr@uni-greifswald.de',
      keywords='lab automation, experiments, database, evaluation, visualisation, SiLA2, robots',
      url='https://gitlab.com/LARAsuite/lara-django',
      license='GPL',
      packages=find_packages(),
      scripts=[],
      install_requires=install_requires,
      test_suite='',
      classifiers=['Development Status :: 3 - Alpha',
                   'Environment :: Console',
                   'Framework :: Django :: 3.1',
                   'Intended Audience :: Developers',
                   'Intended Audience :: Science/Research',
                   'Intended Audience :: Education',
                   'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 3.9',
                   'Programming Language :: Python :: 3.10',
                   'Topic :: Scientific/Engineering',
                   'Topic :: Scientific/Engineering :: Information Analysis',
                   'Topic :: Scientific/Engineering :: Visualization',
                   'Topic :: Scientific/Engineering :: Bio-Informatics',
                   'Topic :: Scientific/Engineering :: Chemistry'
                   ],
      include_package_data=True,
      package_data=package_data,
      data_files=data_files,
      )
