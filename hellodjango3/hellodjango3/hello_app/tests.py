from django.test import TestCase
from django.utils import timezone

from hello_app.models import Note


class NoteModelTests(TestCase):

    def test_note_text(self):
        """
        testing not text
        """
        curr_note = Note(text="hello note")
        self.assertEqual(curr_note.text, "hello note")
