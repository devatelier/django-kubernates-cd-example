from django.db import models


class Note(models.Model):
    id = models.AutoField(primary_key=True)
    text = models.TextField()
