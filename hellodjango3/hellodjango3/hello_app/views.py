from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from django.views import generic

from .models import Note


class IndexView(generic.ListView):
    #template_name = 'polls/index.html'
    #context_object_name = 'latest_question_list'
    model = Note


def homePageView(request):
    html_txt = """ 
    <html>
      <h1>Hellodjango3 app - v0.0.1</h1> 
      
        <p> Hello, World! - nothing more </p>
    </html>
    """
    return HttpResponse(html_txt)
