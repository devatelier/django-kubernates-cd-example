from django.urls import path
from django.contrib import admin
from .views import homePageView, IndexView

app_name = 'hello_app'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    #path('', homePageView, name='home')

]
