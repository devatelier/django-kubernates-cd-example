"""hellodjango3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import logging
from django.contrib import admin
from django.urls import path, include
from django.utils.module_loading import import_string
from django_grpc_framework.settings import grpc_settings


logger = logging.getLogger(__name__)

urlpatterns = [
    path('', include('hello_app.urls')),
    path('todos/', include('todos.urls')),
    path('admin/', admin.site.urls),
]


def grpc_handlers(server):
    """
    Add servicers to the server
    """

    logger.info("adding grpc handlers now")

    if len(grpc_settings.user_settings['GRPC_HANDLERS']) == 0:
        logger.warning(
            "No servicers configured. Did you add GRPC_HANDLERS list to GRPC_FRAMEWORK settings?")

    for handler_str in grpc_settings.user_settings['GRPC_HANDLERS']:
        logger.debug(f"Adding servicers from {handler_str}")
        register_handler = import_string(handler_str)
        register_handler(server)  # calling each handler registration
