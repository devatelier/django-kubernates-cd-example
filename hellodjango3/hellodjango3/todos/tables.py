import django_tables2 as tables

from .models import TaskItem


class TodoTable(tables.Table):
    title = tables.Column(linkify=('todos:todo-detail', [tables.A('pk')]))

    class Meta:
        model = TaskItem

        # auto-generating the column header
        #fields = [field.name for field in TaskItem._meta.get_fields()]

        fields = ("title", "body", "category", "due_date", "task_finished")
