# django gRPC framework handler and serialisation

from django_grpc_framework import generics, proto_serializers

from todos.grpc_interface import taskitem_pb2
from todos.grpc_interface import taskitem_pb2_grpc

from todos.models import TaskItem


class TaskItemProtoSerializer(proto_serializers.ModelProtoSerializer):
    class Meta:
        model = TaskItem
        proto_class = taskitem_pb2.TaskItem
        fields = ['title', 'body', 'due_date']


class TaskItemService(generics.ModelService):
    queryset = TaskItem.objects.all()
    serializer_class = TaskItemProtoSerializer


def grpc_handlers(server):
    taskitem_pb2_grpc.add_TaskItemControllerServicer_to_server(
        TaskItemService.as_servicer(), server)
