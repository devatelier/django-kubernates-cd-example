from django.urls import path
from django_grpc_framework.settings import grpc_settings
from .views import TodoItemListView, TodoItemDetailView, TodoItemUpdateView, TodoItemDeleteView, TodoItemCreateView

app_name = 'todos'

urlpatterns = [
    path('list/', TodoItemListView.as_view(), name='todos_list'),
    path('create/', TodoItemCreateView.as_view(), name='create_item'),
    path('update/<pk>', TodoItemUpdateView.as_view(), name='update_list'),
    path('delete/<pk>', TodoItemDeleteView.as_view(), name='delete_list'),
    path('<pk>/', TodoItemDetailView.as_view(), name='todo-detail'),
]

# register handlers in settings
grpc_settings.user_settings["GRPC_HANDLERS"] = [
    "todos.grpc_interface.handlers.grpc_handlers"]
