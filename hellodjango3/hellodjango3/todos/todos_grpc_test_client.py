import grpc

from grpc_interface import taskitem_pb2
from grpc_interface import taskitem_pb2_grpc

with grpc.insecure_channel('localhost:50051') as channel:
    stub = taskitem_pb2_grpc.TaskItemControllerStub(channel)
    
    for ti in stub.List(taskitem_pb2.TaskItemListRequest()):
        print(ti.title, " -> ", ti.body,  end='\n')