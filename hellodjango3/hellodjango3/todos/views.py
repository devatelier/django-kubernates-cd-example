
from django.shortcuts import redirect, render
from django.views.generic import DetailView
from django.views.generic.edit import CreateView, CreateView, UpdateView, DeleteView
from django_tables2 import SingleTableView

from .models import TaskItem
from .forms import TaskItemCreateForm, TaskItemUpdateForm
from .tables import TodoTable

# Create your views here.


class TodoItemListView(SingleTableView):
    model = TaskItem
    template_name = 'todos/todoitem_list.html'

    table_class = TodoTable

    # def get_context_data(self, **kwargs):
    #     context = super().get_context_data(**kwargs)
    #     context['table_headers'] = [
    #         field.name for field in TaskItem._meta.get_fields()]
    #     return context


class TodoItemDetailView(DetailView):
    model = TaskItem

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #context['now'] = timezone.now()
        context['section-title'] = "ToDo Detail"
        return context


class TodoItemCreateView(CreateView):
    model = TaskItem
    #fields = ['title', 'body', 'due_date', 'task_finised', 'category']

    template_name = 'todos/todoitem_create_form.html'
    form_class = TaskItemCreateForm
    success_url = '/todos/list'


class TodoItemUpdateView(UpdateView):
    model = TaskItem
    template_name = 'todos/todoitem_update_form.html'
    form_class = TaskItemUpdateForm
    success_url = '/todos/list'


class TodoItemDeleteView(DeleteView):
    model = TaskItem
    template_name = 'todos/todoitem_delete_form.html'
    success_url = "/todos/list"
