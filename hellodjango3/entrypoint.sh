#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
    #while ! nc -z $POSTGRES_SERVICE_HOST $POSTGRES_SERVICE_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

# we do not want to run that every time the container starts:
#python manage.py flush --no-input
#python manage.py migrate

# run this once after container is running:
# docker-compose exec django_backend python manage.py flush --no-input
# docker-compose exec django_backend python manage.py migrate

exec "$@"
