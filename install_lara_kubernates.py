#!/usr/bin/env python3
"""_____________________________________________________________________

:PROJECT: lara-django

*lara-django installer skript*

:details: lara-django installer.
          Installing core LARA python-django environment.
          
          - core settings and database
          - 
          

# ask for deployment type (development, staging, production)

# generate local secrets for deployment

# on the fly (no file)

# from file


:authors: mark doerr (mark@uni-greifswald.de)

________________________________________________________________________
"""

import sys
import os
from pathlib import Path
from configparser import ConfigParser, NoSectionError, NoOptionError

import pip
import subprocess

# checking python version
major_version = sys.version_info.major
minor_version = sys.version_info.minor

# this works only with python >= 3.6
print(f"You are running python {major_version}.{minor_version}")

HOME_DIR = str(Path.home())

REPO_DIR = os.path.dirname(os.path.realpath(__file__))  # current directory of this repository

# try reading configuration from system config files
LARA_CONFIG_FILE = default_answer=os.path.join(HOME_DIR, '.config', 'lara-suite', 'lara-django', 'lara-django.conf') 
lara_config_file = False

lara_config = ConfigParser()
try:     
    lara_config.read(LARA_CONFIG_FILE)
    #~ lara_config_file = True
    
except Exception as err :
    sys.stderr.write("ERROR: Please add section in your config file {}".format(err) )
    lara_config_file = False



def install_on_kubernates(inst_mode):

    if inst_mode == 'k':
        print("install on kubernates... now")

    elif inst_mode == 'd':
        install_LARA_venv('venv')


    # sphinx-build could be installed in "$HOME/.local/bin" which is not in PATH by default
    # add it to PATH to avoid any errors
    # os.environ['PATH'] += os.pathsep + os.path.expanduser("~/.local/bin")
    #generate_documentation()



# --------------- installation helper functions, please do not modify -----------------------------

def print_welcome_message():
    """ :param [param_name]: [description]"""

    # Display Welcome Text
    installer_welcome_txt = ("_____________________________________________\n\n"
                            "   __      __    ____    __                  \n"
                            "  (  )    /__\  (  _ \  /__\                 \n"
                            "   )(__  /(__)\  )   / /(__)\                \n"
                            "  (____)(__)(__)(_)\_)(__)(__)               \n\n"
                            "        I N S T A L L E R                    \n"
                            " I will guide you through the complete       \n"
                            " installation of the LARA software suite ... \n"
                            " [type: ?    - for further information       \n"
                            "        help - to list all input options]    \n"
                            "_____________________________________________\n")
    print(installer_welcome_txt)

def show_system_requirements():
    """ :param [param_name]: [description]"""

    # Display Welcome Text
    system_requirements_txt = """
System requirements:
    - kubernates installation
    - helm 3
    - sops
    """
    print(system_requirements_txt)

def str2bool(v):
  # s. https://stackoverflow.com/questions/715417/converting-from-a-string-to-boolean-in-python
  return v.lower() in ("yes", "true", "t", "1")

def query_yes_no(question, default_answer="yes", help=""):
    """Ask user at stdin a yes or no question

    :param question: question text to user
    :param default_answer: should be "yes" or "no"
    :param help: help text string
    :return:  :type: bool
    """
    if default_answer == "yes":
        prompt_txt = "{question} [Y/n] ".format(question=question)
    elif default_answer == "no":  # explicit no
        prompt_txt = "{question} [y/N] ".format(question=question)
    else:
        raise ValueError("default_answer must be 'yes' or 'no'!")

    while True:
        try:
            answer = input(prompt_txt)
            if answer:
                if answer == "?":
                    print(help)
                    continue
                else:
                    return str2bool(answer)
            else:
                return str2bool(default_answer)
        except ValueError:
            sys.stderr.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")
        except KeyboardInterrupt:
            sys.stderr.write("Query interrupted by user, exiting now ...")
            exit(0)


def query(question, default_answer="", help=""):
    """Ask user a question

    :param question: question text to user
    :param default_answer: any default answering text string
    :param help:  help text string
    :return: stripped answer string
    """
    prompt_txt = "{question} [{default_answer}] ".format(question=question, default_answer=default_answer)

    while True:
        answer = input(prompt_txt).strip()

        if answer:
            if answer == "?":
                print(help)
                continue
            else:
                return answer
        else:
            return default_answer


def call(command=""):
    ''' Convenient command call: it splits the command string into tokens (default separator: space)

    :param command: the command to be executed by the system
    '''
    try:
        cmd_lst = command.split()

        print("cmd lst {}".format(cmd_lst))

        subprocess.run(cmd_lst, check=True)
    except subprocess.CalledProcessError as err:
        sys.stderr.write('CalledProcessERROR:{}'.format(err))


def run(command="", parameters=[]):
    '''This version is closer to the subprocess version

    :param command: the command to be executed by the system
    :param parameters: parameters of the this command
    '''
    try:
        subprocess.run([command] + parameters, check=True, shell=True)
    except subprocess.CalledProcessError as err:
        sys.stderr.write('ERROR:', err)


def runSetup(src_dir="", lib_dir=""):
    """running a setup.py file within a pyhton script
       it requires a lot of things set...
       :param src_dir: directory containing the setup.py file
       :param lib_dir: directory containing the target lib directory
    """
    # all path settings seem to be required by run_setup and setup.py
    os.environ["PYTHONPATH"] = os.path.join(lib_dir, 'lib', 'python3.10', 'site-packages')
    sys.path.append(lib_dir)
    os.chdir(src_dir)
    setup_file = os.path.join(src_dir, 'setup.py')
    # this no longer works:
    #~ run_setup(setup_file,  script_args=['install', '--prefix', lib_dir ])
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', '.'])

def installAndImport(package):
    """ This imports a package and
        if not available, installs it first

        :return: The sucessfully imported package
    """
    import importlib
    try:
        importlib.import_module(package)
    except ImportError:
        import pip
        #~ pip.main(['install', package])
        print(f"Module {package} not installed! I'm going to install it now...")
        subprocess.check_call([sys.executable, '-m', 'pip', 'install', package])
    finally:
        globals()[package] = importlib.import_module(package)
        return globals()[package]

def generate_documentation(venv_dir=""):
    """ :param [param_name]: [description]"""

    # Generate documentation ?
    if query_yes_no("Generate documentation (all documentation can be also found at \n https://sila2.gitlab.io/sila_python) ?",
                    help="HELP: This will generate the documentation"):
        print("Generating documentation ...")
        installAndImport("sphinx") # if not installed, there will be an error
        installAndImport("python_docs_theme") # nice python documentation theme ... comes with python 3.7
        docs_dir = os.path.join(REPO_DIR, 'docs')
        os.chdir(docs_dir)
        call('make html')

def install_LARA_venv(venv_module_name="venv"):
    """ :param venv_module_name: The module to use for creating the virtual environment
                            Possible values: 'venv' for the venv module that ships with python (default, works on Linux)
                                             'virtualenv' (works on Windows)
    """
    if query_yes_no("Install a virtual Python environment (highly recommended) ?",
                     help="HELP: This is the recommended installation mode for testing LARA-django"):

        venv_dir = query("Please specify a directory for your virtual python3 environment",
                         default_answer=os.path.join(HOME_DIR, "py3venv", "lara"),
                         help="HELP: specify the target directory for the virtual python3 environment")

        create_venv_anyway = True

        if os.path.exists(venv_dir):
            create_venv_anyway = query_yes_no("\nWARNING !! Virtual environment exists: [{}], shall I create it anyway ?".
                                              format(venv_dir), default_answer='no',
                                              help="HELP: create the python 3 virtual environment anyway ?")

        if create_venv_anyway:
            venv_module = installAndImport(venv_module_name)

            print("\t...creating virtual environment using '{module}' in dir [{dir}]".format(
                module=venv_module_name,
                dir=venv_dir
            ))
            if venv_module is not None:
                if venv_module_name == 'venv':
                    venv_module.create(venv_dir, system_site_packages=False, clear=False, symlinks=False, with_pip=True)
                elif venv_module_name == 'virtualenv':
                    venv_module.create_environment(venv_dir, site_packages=False, clear=False, symlink=False)

        print("* Activating virtual environment [{}]".format(
            venv_dir))  # this is done by prepending python3 path to system path
        os.environ['PATH'] = os.pathsep.join([os.path.join(venv_dir, 'bin'), os.environ['PATH']])

        activate_cmd = f"source {venv_dir}/bin/activate"

        if os.name == 'nt':
            print("* Activating virtual environment on windows [{}]".format(
                venv_dir))  # this is done by prepending python3 path to sytem path

            activate_this = os.path.join(venv_dir, "Scripts", "activate_this.py")
            exec(open(activate_this).read(), {'__file__': activate_this})

            # on Windows you need to use quotes when there are spaces in the path
            activate_cmd = f"\"{os.path.join(venv_dir, 'Scripts', 'activate.bat')}\""
            activate_cmd_ps = os.path.join(venv_dir, 'Scripts', 'Activate.ps1')
            activate_cmd += f"\n Powershell users, please use: \"{activate_cmd_ps}\" \n"

        print(" ")
        print("--------------------------------------------------------------------")
        print( "ATTENTION: Please do not forget to activate the virtual environment by calling: \n"
              f"{activate_cmd} \n"
               "- to deactivate the venv, simply type:\n"
               "deactivate \n")
        print("--------------------------------------------------------------------")
        print(" ")

        return venv_dir

def install_on_MSWin(inst_mode):
     print("Installation on MSWndows is not supported yet.")
     show_system_requirements()

if __name__ == '__main__':
    """Main: """
    print_welcome_message()
    show_system_requirements()
    

    # user vs developer

    inst_mode = query("Install LARA on kubernates cluster (default) [k]  \n or setup full development environment [d] ?",
                      default_answer="k",
                      help="HELP: For just using LARA, please select 'k', in case you want to develop software for LARA, select 'd'.")

    if os.name == 'nt':
        install_on_MSWin(inst_mode)
    else:
        install_on_kubernates(inst_mode)

    print("Enjoy the LARAsuite :) ...")