# installation of gitlab runner

s. [gitlab runner on youtube](https://www.youtube.com/watch?v=8Ao5WcMJJ2c)

and 

[runner install](https://docs.gitlab.com/runner/install/)

There are several ways to install a gitlab runner on a kubernates cluster:

1. gitlab runner helm chart

https://docs.gitlab.com/runner/install/kubernetes.html#configuring-gitlab-runner-using-the-helm-chart

the 

1. download the values.yml file (wget https://gitlab.com/gitlab-org/charts/gitlab-runner/blob/main/values.yaml )

1. add gitlab url : 
    gitlabUrl: https://gitlab.com/
  add token: runnerRegistrationToken: 
  * set rbac:
  create: true
  * uncomment the rules section
  do not forget to remove the empty array [] after rules

1. create namespace (demo_namespace.yml) 
this can also be done with the --create-namespace option of the helm command

apiVersion: v1
kind: Namespace
metadata:
    name: sample-helm-cicd-demo

kubectl apply -f demo_namespace.yml

1. run helm3 install
kubectl helm3 install --namespace sample-helm-cicd-demo  gitlab-runner -f helm/gitlab_runner/values.yaml  gitlab/gitlab-runner

1. check
 kubectl describe pods gitlab-runner --namespace=sample-helm-cicd-demo