# kubernates


s. https://microk8s.io/docs/registry-images

list pods
    microk8s kubectl get pods

list namespaces 
     microk8s ctr ns ls

list containers
    microk8s ctr images ls


## Helm

install helm3 with microk8s

microk8s enable helm3



# install helm from gitlab
# s. https://docs.gitlab.com/13.11/ee/user/packages/container_registry/index.html

git commit -am "initial release"
git tag v0.1.0
git push && git push --tags


 # see  https://medium.com/@msvechla/distributing-helm-charts-via-gitlab-container-registry-a87ed2893647
export HELM_EXPERIMENTAL_OCI=1
microk8s helm3 chart pull registry.gitlab.com/devatelier/django-kubernates-cd-example/hellodjango3:v0.0.3
microk8s helm3 chart export registry.gitlab.com/devatelier/django-kubernates-cd-example/hellodjango3:v0.0.3 -d /tmp/

microk8s helm3 install hellodjango3 /tmp/hellodjango3
# later
microk8s helm3 upgrade --install hellodjango3 /tmp/hellodjango3

## working with helm

see https://helm.sh/docs/topics/registries/ 

helm chart list


# sops for secret encryption

Install sops from github release debian package:

https://github.com/mozilla/sops/releases

encrypt secret file, using sops:


## kubernates dashboard



## redis
s. helm repo add bitnami https://charts.bitnami.com/bitnami

microk8s helm3 repo add bitnami https://charts.bitnami.com/bitnami

#microk8s helm3 repo add hellodjango3 registry.gitlab.com/devatelier/django-kubernates-cd-example/hellodjango3

microk8s helm3 chart pull registry.gitlab.com/devatelier/django-kubernates-cd-example/hellodjango3:v0.0.4
microk8s helm3 chart export registry.gitlab.com/devatelier/django-kubernates-cd-example/hellodjango3:v0.0.4 -d /tmp/

helm dependency update /tmp/hellodjango3

microk8s helm3 install hellodjango3 /tmp/hellodjango3


##  remove
microk8s helm3 delete hellodjango3
