
see nice tutorial:
https://testdriven.io/blog/dockerizing-django-with-postgres-gunicorn-and-nginx/

## pulling the images
docker-compose pull

# build docker instances (images) and start
docker-compose up --build


# start in foreground
docker-compose up


# remove volumes and orphans
docker-compose down -v --remove-orphans


## make migrations

while container are running execute in annother shell within the directory containing the docker-compose.yml file:

    docker-compose exec backend_django python manage.py migrate --noinput


# inspecting the volume
docker volume inspect hellodjango3_postgres_data

## production run

docker-compose -f docker-compose -f docker-compose.prod.yml down -v

docker-compose -f docker-compose -f docker-compose.prod.yml up --build