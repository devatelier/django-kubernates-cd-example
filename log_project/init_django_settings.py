"""_____________________________________________________________________

:PROJECT: LARA

* python-django settings generator *

:details: python-django settings generator.
          This module creates a django settings file from a given template and 
          environment variables.
        

:authors: mark doerr mark.doerr@uni-greifswald.de

:date: (creation)         20200413

.. note:: -
.. todo:: -
________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import logging
import argparse
import django
from django.conf import settings
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.dirname(os.path.abspath(__file__))],
    }
]
#  calling settings.configure()  is required to run django modules standalone,
# s. https://docs.djangoproject.com/en/3.0/topics/settings/#calling-django-setup-is-required-for-standalone-django-usage
settings.configure(TEMPLATES=TEMPLATES)
django.setup()
from django.template.loader import get_template
from django.template import Context

from django.core.management.utils import get_random_secret_key

def get_env_value(env_variable):
    try:
      	return os.environ[env_variable]
    except KeyError:
        error_msg = 'Set the {} environment variable'.format(var_name)
        raise ImproperlyConfigured(error_msg)

#os.environ['DATABASE_NAME']
#os.environ['DATABASE_HOST']
#os.environ['DATABASE_PORT']

settings_environment = { "PROJECT_NAME": "lara-django",
                         # database
                         "DATABASE_NAME": "lara_django_db",
                         "DATABASE_HOST": "localhost",
                         "DATABASE_PORT": "5392",
                       }


# add installed apps



# production

# staging

# development


## nplus one https://github.com/jmcarp/nplusone

# testing


#create db 

#init_db

# redis cache: s. https://www.capside.com/labs/deploying-full-django-stack-with-docker-compose/
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/0",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"


# set media files 

# set static files 

# installed apps (iterate throuhg yaml config)

#load fixtures


#python manage.py createsuperuser




def write_settings_file(template, django_settings_filename="django_settings.py"):
    with open(django_settings_filename, 'w') as settings_file:
        django_settings = template.render(settings_environment)

        print(django_settings)
        settings_file.write(django_settings)

# create secret key
def add_django_secret2environment(env_path='/etc/environment'):

    # with open(env_path, 'a') as env_file:
    # base64 encoding ?
    #     env_file.write(f"SECRET_KEY={get_random_secret_key()}")
    # check, if it exists
    print(env_path)
    print(f"SECRET_KEY='{get_random_secret_key()}'")

    


def parse_command_line():
    """
    Just looking for command line arguments
    """
    parser = argparse.ArgumentParser(description="Django Settings generator")
    parser.add_argument('--template',  action='store',
                        default="django_settings.py-tpl", help='django settings template name')

    parser.add_argument('--output',  action='store',
                        default="django_settings.py", help='django settings output')

    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)

    return parser.parse_args()


if __name__ == '__main__':
    # or use logging.INFO (=20) or logging.ERROR (=30) for less output
    logging.basicConfig(format='%(levelname)-8s| %(module)s.%(funcName)s: %(message)s', level=logging.DEBUG)

    parsed_args = parse_command_line()

    write_settings_file(get_template(parsed_args.template))

    add_django_secret2environment()