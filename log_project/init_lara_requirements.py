"""_____________________________________________________________________

:PROJECT: LARA

* python-django requirements generator *

:details: python-django requirements generator.
          This module creates a django requiremnts file from a given template.

:authors: mark doerr mark.doerr@uni-greifswald.de

:date: (creation)         20200415
________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import sys
import argparse
import yaml


def write_reqfile(lara_apps, apps_selection, req_filename):
    with open(lara_apps, 'r') as yaml_file:
        apps_dict = yaml.load(yaml_file, Loader=yaml.SafeLoader)

    try:
        with open(req_filename, 'w') as req_file:
            req_file.write('\n'.join(apps_dict[apps_selection]))
    except KeyError as err:
        newline = '\n\t'
        sys.stderr.write(f"ERROR ({err}): section '{apps_selection}' not found in {lara_apps} \n possible sections are:\n\t{newline.join(apps_dict.keys())}\n")

def parse_command_line():
    """
    Just looking for command line arguments
    """
    parser = argparse.ArgumentParser(description="Django Settings generator")
    parser.add_argument('-a','--lara-apps', action='store',
                        default="lara_django_apps.yml", help='path to lara_django_apps yaml file')

    parser.add_argument('-s', '--apps-selection', action='store',
                        default="lara-django-full", help='selection of django apps')

    parser.add_argument('-o', '--output', action='store',
                        default=os.path.join('requirements','lara_django_apps.txt'), help='lara django apps output')

    parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + __version__)

    return parser.parse_args()


if __name__ == '__main__':
    parsed_args = parse_command_line()

    write_reqfile(parsed_args.lara_apps, parsed_args.apps_selection, parsed_args.output)