
LARAsuite Docker Installation
=============================

LARAsuite relies heavily on container technology to run this complex set of applications in a very well defined and robust environment.

Each functional component (database, django backend, file-cache, frontend and webserver are encapsulated in docker containers)

These docker containers are produced from docker images (= template or blueprint if the container).
The description of how such an image shall be build is specified in a corresponding docker file.

In case we are using standard images with only litte modifications, the images are directly pulled from a docker repository (e.g. DockerHub).
This is e.g. the case for the Redis and PostgreSQL images.

In a development environment docker-compose can be used to build all images and run them on a local machine.
In production this approach is not reliable and flexible enough. Therefore kubernates is used to configure, run, orchestrate and monitor this set of containers, 
which is also called a kubernates pod - inspired by the term for a group of wales living together (a wale is the logo of docker.com).

To make kubernates installations / deployments portable, Helm-charts specify the complete and complex deployment process. 
By that the LARAsuite with all components can be installed with one single command.


The LARA docker installation consists of the followining containers:

- lara-django 
- PostgreSQL
- Redis  (for caching)
- ngix / frontent

There are at least the following configurations:

production (default)
development
testing
staging (for demonstration installations, )

## Secret handlings

secret keys shall not be stored in configuration files, they are retrieved from environment variables.
s.
https://kubernetes.io/docs/tasks/inject-data-application/distribute-credentials-secure/


python manage.py createsuperuser

## environment variables

https://docs.docker.com/compose/environment-variables/
https://docs.docker.com/compose/env-file/
https://vsupalov.com/docker-arg-env-variable-guide/
https://vsupalov.com/docker-arg-env-variable-guide/#the-dot-env-file-env


A list of all environment variables to be set is in lara_django.env ??

## django static files handlings

https://www.capside.com/labs/deploying-full-django-stack-with-docker-compose/

## building the lara docker image 

Hint: to run docker with user rights, please add the user to the docker group

.. code-block:: bash
   sudo addgroup my_user_name docker # the user needs to re-login to activate this

.. code-block:: bash

   docker image build -t larasuite/lara_mini .

## interacting with the container

 docker run -i --rm postgres ls -Al /


## running the LARA container

.. code-block:: bash

   docker container run -p 9999:8888 larasuite/lara-django

   docker-compose exec backend python manage.py makemigrations logApp
   docker-compose exec backend python manage.py migrate


   docker-compose exec backend python manage.py collectstatic


## publishing a container on DockerHub

.. code-block:: bash

   sudo docker login
   sudo docker image push larasuite/lara-django

## building the all images with docker-compose

Hint: please make sure, that docker compose is installed on your system

.. code-block:: bash
   docker-compose build

   # running everything
   docker-compose up # use -d option to run in background
   
   # list all docker processes
   docker ps
   docker-compose ps

   # stop everything
   docker-compose down 

   # check all processes running
   docker-compose ps

## execute command in container

run vs exec



## docker cleaning up

docker volume ls

docker volume rm {volume_name}
docker volume prune # deletes them all 



## Assesment of deployment strategies

To find the best deployment strategy, the follwing source have been considered:

### django settings

https://www.toptal.com/django/django-top-10-mistakes

https://github.com/sobolevn/django-split-settings

https://sobolevn.me/2017/04/managing-djangos-settings

### docker

https://github.com/wemake-services/wemake-django-template

mayan-edms

https://github.com/pypa/warehouse/blob/master/Dockerfile


### kubernates

https://github.com/wemake-services/wemake-django-template

https://verbose-equals-true.gitlab.io/django-postgres-vue-gitlab-ecs/start/overview/#high-level-overview

https://github.com/waprin/kubernetes_django_postgres_redis
