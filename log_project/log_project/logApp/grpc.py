from . import helloworld_pb2_grpc
from .hello_servicer import Greeter


def register_servicer(server):
    """ Callback for django_grpc """
    helloworld_pb2_grpc.add_GreeterServicer_to_server(Greeter(), server)
