# django-kubernates-cd-example

python django  continous delivery to kubernates example 


## docker-compose

 
    # local procduction server
    
    # local development server 
    docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --build

## gRPC

### proto generation

    mkdir protos
    python manage.py generateproto --model todos.models.TaskItem --fields title,body,due_date --file todos/protos/taskitem.proto    

### gRPC stub generation

    mkdir grpc_interface
    python -m grpc_tools.protoc --proto_path=./todos/protos --python_out=./todos/grpc_interface --grpc_python_out=./todos/grpc_interface ./todos/protos/taskitem.proto

## gRPC web server

    python manage.py grpcrunserver --dev

### gRPC client

s. todos_demo_client.py




Acknowledgements:

todo-list app is inspired by:
https://betterprogramming.pub/how-to-build-a-todo-application-with-django-ac675811d77b